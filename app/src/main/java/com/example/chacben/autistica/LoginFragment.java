package com.example.chacben.autistica;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.os.SystemClock;
import android.support.design.widget.NavigationView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;

import java.security.Policy;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    OnSuccessListener successListener;

    ProgressBar progressBar;
    CheckBox checkbox;

    public interface OnSuccessListener {
        public void OnSuccessLogin(boolean success);
    }

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_login, container, false);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        checkbox = (CheckBox) view.findViewById(R.id.checkBox);
        Button loginButton = (Button) view.findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowSpinnerTask task = new ShowSpinnerTask();
                final AsyncTask<String, String, String> execute = task.execute();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            successListener = (OnSuccessListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnItemClickedListener");
        }
    }

    class ShowSpinnerTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(ProgressBar.GONE);
            successListener.OnSuccessLogin(true);
            if(checkbox.isChecked()) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_main, new DashboardFragment())
                        .commit();
            } else {
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_main, new FragmentEmoji())
                        .addToBackStack("emoji")
                        .commit();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(ProgressBar.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            SystemClock.sleep(3000);
            return null;
        }
    }
}
