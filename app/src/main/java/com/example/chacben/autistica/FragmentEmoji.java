package com.example.chacben.autistica;


import android.app.FragmentManager;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentEmoji extends Fragment {

    public FragmentEmoji() {
        // Required empty public constructor
    }

    class EmojiClickListener implements View.OnClickListener {
        private int id = 0;
        public EmojiClickListener(int id) {
            this.id = id;
        }
        @Override
        public void onClick(View view) {
            navigate();
        }
        FragmentManager fragmentManager = getFragmentManager();
        private void navigate() {
            switch (id) {
                case 0:
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.content_main, new FragmentC())
                            .addToBackStack(null)
                            .commit();
                    break;
                case 1:
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.content_main, new FragmentNervous())
                            .addToBackStack(null)
                            .commit();
                    break;
                case 2:
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.content_main, new FragmentA_sad())
                            .addToBackStack(null)
                            .commit();
                    break;
                case 3:
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.content_main, new FragmentC())
                            .addToBackStack(null)
                            .commit();
                    break;
                case 4:
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.content_main, new FragmentC())
                            .addToBackStack(null)
                            .commit();
                    break;
                case 5:
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.content_main, new FragmentC())
                            .addToBackStack(null)
                            .commit();
                    break;
                case 6:
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.content_main, new FragmentC())
                            .addToBackStack(null)
                            .commit();
                    break;
                case 7:
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.content_main, new FragmentC())
                            .addToBackStack(null).commit();
                    break;
                case 8:
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.content_main, new FragmentA_happy())
                            .addToBackStack(null)
                            .commit();
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment_emoji, container, false);
        List<ImageView> imgs = new ArrayList<>();
        imgs.add((ImageView) v.findViewById(R.id.emoji01));
        imgs.add((ImageView) v.findViewById(R.id.emoji02));
        imgs.add((ImageView) v.findViewById(R.id.emoji03));
        imgs.add((ImageView) v.findViewById(R.id.emoji04));
        imgs.add((ImageView) v.findViewById(R.id.emoji05));
        imgs.add((ImageView) v.findViewById(R.id.emoji06));
        imgs.add((ImageView) v.findViewById(R.id.emoji07));
        imgs.add((ImageView) v.findViewById(R.id.emoji08));
        imgs.add((ImageView) v.findViewById(R.id.emoji09));
        int i = 0;
        for (ImageView iv : imgs) {
            iv.setOnClickListener(new EmojiClickListener(i++));
        }
        return v;
    }

}
