package com.example.chacben.autistica;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNervousSymptom extends Fragment {


    public FragmentNervousSymptom() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nervous_symptom, container, false);
        final Button buttonYes = (Button) view.findViewById(R.id.buttonYes);
        buttonYes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_main, new FragmentB())
                        .addToBackStack("FragmentNervousSymptom").commit();
            }
        });

        final Button buttonNo = (Button) view.findViewById(R.id.buttonNo);
        buttonNo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_main, new FragmentNervous())
                        .addToBackStack("FragmentNervousSymptom").commit();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }
}
