package com.example.chacben.autistica;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentA_sad extends Fragment {


    public FragmentA_sad() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragmenta_sad, container, false);
        View view = inflater.inflate(R.layout.fragmenta_sad, container, false);
        final Button buttonYes = (Button) view.findViewById(R.id.button);
        buttonYes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.content_main, new FragmentB())
                        .addToBackStack("FragmentA_sad").commit();
            }
        });

        final Button buttonSad = (Button) view.findViewById(R.id.button2);
        buttonSad.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.content_main, new FragmentA_sad2())
                        .addToBackStack("FragmentA_sad").commit();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

 }





