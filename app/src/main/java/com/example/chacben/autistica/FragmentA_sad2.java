package com.example.chacben.autistica;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentA_sad2 extends Fragment {


    public FragmentA_sad2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragmenta_sad, container, false);

        View view = inflater.inflate(R.layout.fragment_a_sad2, container, false);
        final Button buttonHappy = (Button) view.findViewById(R.id.button);
        buttonHappy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.content_main, new FragmentB())
                        .addToBackStack("FragmentA_sad2").commit();
            }
        });

        final Button buttonSad = (Button) view.findViewById(R.id.button2);
        buttonSad.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.content_main, new FragmentEmoji())
                        .addToBackStack("FragmentA_sad2").commit();
            }
        });


        // Inflate the layout for this fragment
        return view;
   }

}

