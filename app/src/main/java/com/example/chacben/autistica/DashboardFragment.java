package com.example.chacben.autistica;


import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Arrays;
import java.util.List;

import static com.example.chacben.autistica.R.id.speedometer;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {
    ListView listView;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        SpeedometerGauge speedometerGauge = (SpeedometerGauge) view.findViewById(R.id.speedometer);
        // configure value range and ticks
        speedometerGauge.setMaxSpeed(100);
        speedometerGauge.setMajorTickStep(30);
        speedometerGauge.setMinorTicks(2);

        // Configure value range colors
        speedometerGauge.addColoredRange(30, 140, Color.GREEN);
        speedometerGauge.addColoredRange(140, 180, Color.YELLOW);
        speedometerGauge.addColoredRange(180, 400, Color.RED);

        listView = (ListView) view.findViewById(R.id.listView);
        String[] stringArray = getResources().getStringArray(R.array.list_moments);
        List<String> listItems = Arrays.asList(stringArray);



        return view;
    }

}
