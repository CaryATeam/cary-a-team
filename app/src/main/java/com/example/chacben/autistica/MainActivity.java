package com.example.chacben.autistica;

import android.app.Fragment;
import android.app.FragmentManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
class MyBounceInterpolator implements android.view.animation.Interpolator {
    double mAmplitude = 1;
    double mFrequency = 10;

    MyBounceInterpolator(double amplitude, double frequency) {
        mAmplitude = amplitude;
        mFrequency = frequency;
    }

    public float getInterpolation(float time) {
        return (float) (-1 * Math.pow(Math.E, -time/ mAmplitude) *
                Math.cos(mFrequency * time) + 1);
    }
}
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, LoginFragment.OnSuccessListener  {
    private static final String defaultProfileImg = "http://www.lmbu.dk/media/2305/default_profile_pic.jpg?anchor=center&mode=crop&width=200&height=200&rnd=130909426290000000";
    private static final String userProfileImg = "https://crunchbase-production-res.cloudinary.com/image/upload/c_limit,h_600,w_600/v1452103813/ldsxh2b4nrzob7wmlqiu.png";

    private View navHeader;
    private ImageView imgProfile;
    private static MediaPlayer module1Player, module2Player, module3Player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        module1Player = MediaPlayer.create(this, R.raw.rainforest1);
        module1Player.setVolume(1.0f, 1.0f);
        module1Player.setLooping(true);
        module2Player = MediaPlayer.create(this, R.raw.rainforest2);
        module2Player.setVolume(1.0f, 1.0f);
        module2Player.setLooping(true);
        module3Player = MediaPlayer.create(this, R.raw.oceansound4);
        module3Player.setVolume(1.0f, 1.0f);
        module3Player.setLooping(true);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Menu navMenu = navigationView.getMenu();
        navMenu.setGroupVisible(R.id.drawer_group, false);
        navHeader = navigationView.getHeaderView(0);
        imgProfile = (ImageView) navHeader.findViewById(R.id.profileImage);

        // Loading profile image
        Glide.with(this).load(defaultProfileImg)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgProfile);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content_main, new LoginFragment())
                .commit();
        setTitle("Login");
    }

    @Override
    public void OnSuccessLogin(boolean success) {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu navMenu = navigationView.getMenu();
        navMenu.setGroupVisible(R.id.drawer_group, success);
        navHeader = navigationView.getHeaderView(0);
        TextView userName = (TextView) navHeader.findViewById(R.id.nav_user);
        userName.setText("Kim Hammonds");
        TextView email = (TextView) navHeader.findViewById(R.id.nav_email);
        email.setText("kim.hammonds@xyz.com");
        imgProfile = (ImageView) navHeader.findViewById(R.id.profileImage);
        Glide.with(this).load(userProfileImg)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgProfile);
        setTitle("Dashboard");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_help) {
            Toast.makeText(getApplicationContext(), "Help me!", Toast.LENGTH_LONG);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = new FragmentA();

        if(module1Player.isPlaying()) {
            module1Player.pause();
        }
        if(module2Player.isPlaying()) {
            module2Player.pause();
        }
        if(module3Player.isPlaying()) {
            module3Player.pause();
        }

        if(id == R.id.nav_dashboard) {
            fragment = new DashboardFragment();
            setTitle("Dashboard");
        }else if (id == R.id.nav_camera) {
            fragment = new FragmentEmoji();
            setTitle("Emotions");
            module1Player.start();
        } else if (id == R.id.nav_gallery) {
            fragment = new FragmentB();
            setTitle(getResources().getString(R.string.nav_calander));
            module2Player.start();
        } else if (id == R.id.nav_slideshow) {
            fragment = new FragmentC();
            setTitle(getResources().getString(R.string.nav_locations));
            module3Player.start();
        } else if (id == R.id.nav_manage) {
            fragment = new FragmentEmoji();
            setTitle("Emotions");
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        fragmentManager
                .beginTransaction()
                .replace(R.id.content_main, fragment)
                .addToBackStack(fragment.toString())
                .commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void didTapButton(View view) {
        ImageButton button = (ImageButton)findViewById(R.id.imageButton11);
        //ImageButton button2 = (ImageButton)findViewById(R.id.imageButton12);
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.animator.bounce);


        // Use bounce interpolator with amplitude 0.2 and frequency 20
         MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
         myAnim.setInterpolator(interpolator);

        button.startAnimation(myAnim);
        //button2.startAnimation(myAnim);
    }
}
